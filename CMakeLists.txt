# The minimum version is required. Using a lower cmake version may not work at
# all.
cmake_minimum_required(VERSION 3.19)

# I don't see this being refined much further, so we can call it 1.0.
project(IRTool VERSION 1.0)

# There isn't any C code anyway, so there is no real need to enable C as well
# although you might see both languages being enabled in similar situations.
enable_language(CXX)

# Enable colors for compiler error messages. Didn't think this had to be done
# explicitly with Ninja, but apparently it does. Not sure if it is needed with
# other generators (Makefiles).
set(CMAKE_COLOR_DIAGONSTICS ON CACHE BOOL
  "Use color for compiler diagnostics.")

# If LLVM is installed in a non-standard location, provide that location.
# Setting this option will disable searches for LLVM on the default paths, so
# if a valid LLVM installation is not found at the specified path, it will
# result in a configuration time error. The path provided here should be the
# top-level install prefix. It should contain bin/ and lib/ subdirectories. The
# bin should contain an llvm-config executable.
option(WITH_LLVM "Path to LLVM's install prefix." "")

# LLVM has no problem breaking API's and do so with unfailing regularity. Alas,
# we need to be strict about what versions we can support. The LLVM_MAX_VERSION
# variable must be the first version that is *not* supported.
set(LLVM_MIN_VERSION 15.0)
set(LLVM_MAX_VERSION 16.0)

# If WITH_LLVM is used, don't look anywhere else for the package. We want to
# make sure that a different installation is not picked up because it might
# be a different - and incompatible - version.
#
if (WITH_LLVM)
  find_package(LLVM ${LLVM_MIN_VERSION}...<${LLVM_MAX_VERSION}
    REQUIRED CONFIG PATHS ${WITH_LLVM} NO_DEFAULT_PATH)
else ()
  find_package(LLVM ${LLVM_MIN_VERSION}...<${LLVM_MAX_VERSION}
    REQUIRED CONFIG)
endif()
message(STATUS "Found LLVM: ${LLVM_VERSION}")

# We will link against libLLVM-<version>.so (see comment associated with the
# call to target_link_libraries below). Most modern LLVM installations
# (including those provided by your distribution) should have it. Some systems
# may also provide a libLLVM.so which is symlinked to libLLVM-<version>.so
# On MacOS, the dynamic library extension is .dylib, not .so as it is on Linux
# and the BSD's.
find_library(LIBLLVM LLVM-${LLVM_VERSION_MAJOR}
  REQUIRED PATHS ${LLVM_LIBRARY_DIR} NO_DEFAULT_PATH)
message(STATUS "Found libLLVM-${LLVM_VERSION_MAJOR}: ${LLVM_LIBRARY_DIR}")

# If using other (non-LLVM) libraries, add those here.
#
# find_package(name of gee-whiz package)

# Run llvm-config to find the correct compiler-flags to be used to compile code
# that will be linked with LLVM.
execute_process(COMMAND ${LLVM_TOOLS_BINARY_DIR}/llvm-config "--cxxflags"
  OUTPUT_VARIABLE LLVM_CXX_FLAGS
  OUTPUT_STRIP_TRAILING_WHITESPACE)

# Include -Wall and -Wextra by default in my build files because it can help
# catch some kinds of bugs early.
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra ${LLVM_CXX_FLAGS}")

# Since LLVM is required, the variables below should be set correctly.
add_definitions(${LLVM_DEFINITIONS})

# Since both -Wall and -Wextra are set, there may be warnings from LLVM's own
# headers. Depending on how many headers are (transitively) included, this can
# get very annoying. By marking the LLVM headers as "system" headers, those
# warnings can be silenced.
include_directories(${LLVM_INCLUDE_DIRS} SYSTEM)

# This is the directory that should contain all the LLVM libraries that were
# built.
link_directories(${LLVM_LIBRARY_DIR})

# The actual tool being built.
set(EXE ir-tool)
add_executable(${EXE} IRTool.cpp)

# Many examples in LLVM's documentation will link against the static
# libraries for the subcomponents of LLVM. This is useful if you know exactly
# which components you need. Linking statically can also lower startup time of
# the tool. However, it can increase the compile-time of the tool, especially
# if you end up using many of the static libraries.
#
# The other option is to link against a single libLLVM.so that contains all of
# the components of LLVM. The advantage is that the compilation of the tool
# itself will be faster which is useful during development. The startup time
# will be increased slightly because the library would have to be loaded each
# time the tool is run.
#
# Here, we are linking against the dynamic shared library because it is easier.
# LLVM's documentation will have examples of linking against the individual
# static components.
target_link_libraries(${EXE} LLVM)
