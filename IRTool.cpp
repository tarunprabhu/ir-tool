// Tarun Prabhu <tarun@lanl.gov> [08-Feb-2023]

// If the indentation in this file appears strange or otherwise inconsistent,
// it may be safe to blame clang-format for doing odd things.

#include <getopt.h>

#include <algorithm>

#include <llvm/IR/InstIterator.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/MemoryBufferRef.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/WithColor.h>
#include <llvm/Support/raw_ostream.h>

using namespace llvm;

// Type aliases to make some things shorter.
using Colors = raw_fd_ostream::Colors;

// These values will be set when the command-line is parsed. static to reinforce
// the fact that it is only intended for use in this file.
static bool verbose = false;

// Command-line argument specs.
static const struct option opts[] {
  // Print a help message and exit.
  {"help", no_argument, nullptr, 'h'},

      // Enable verbose mode.
      {"verbose", no_argument, nullptr, 'v'},

      // Write resulting module to file instead of stdout. If the tool does not
      // change the module at all, the output will be identical to the input.
      {"outfile", required_argument, nullptr, 'o'},

      // The last struct full of zeros is necessary to indicate the end of the
      // opts array.
      {0, 0, 0, 0},
};

// Return the message stream. This should be called when writing a "logging"
// message. If verbose mode is enabled, write a label to the stream and return
// the stream. If verbose mode is not enabled, return a null stream which will
// simply ignore anything passed to it. There is, therefore, no need to check
// if verbose mode is enabled when logging a message.
raw_ostream &message() {
  if (verbose) {
    WithColor(outs(), Colors::GREEN, true) << "ir-tool";
    outs() << ": ";
    return outs();
  } else {
    return nulls();
  }
}

// The heart of the work being done is launched from this function. Do
// interesting things with the supplied module.
int runTool(Module &m) {
  // This is just a placeholder to get the code in this file to actually do
  // something. But you should replace this call with whatever you need to do.

  // The code in the example section after the main function demonstrates how
  // you can iterate over some of the contents of the module.
  int runOnModule(Module &);
  return runOnModule(m);
}

static void printUsage() {
  const char* help = R"DOC(ir-tool [-h] [-o <outfile>] <file>

Do something with the given LLVM IR file.

OPTIONS

   -h                 Print this help message.
   --help

   -o        <file>   The file to which to save the module after all the tools
   --outfile          have been run. Defaults to stdout.

   -v                 Enable verbose mode.
   --verbose

ARGUMENTS

   <file>             Path to the LLVM IR file from which to load the LLVM
                      module. This may be either an LLVM assembly (.ll) or
                      bitcode (.bc) file.
)DOC";
  errs() << help << "\n";
}

int main(int argc, char *argv[]) {
  // These will be set after the command-line options have been parsed.
  std::string irFile;
  std::string outFile;

  // Parse command-line options.
  while (true) {
    int optIdx = 0;
    int c = getopt_long(argc, argv, "ho:v", opts, &optIdx);
    if (c == -1)
      break;

    switch (c) {
    case 'h':
      printUsage();
      exit(EXIT_SUCCESS);
    case 'o':
      outFile = optarg;
      break;
    case 'v':
      verbose = true;
      break;
    default:
      // The error message will already have been printed by getopt_long.
      // So just exit with an error code.
      exit(EXIT_FAILURE);
    }
  }

  // Parse command-line arguments.
  if (argc == 1 or optind == argc) {
    WithColor::error() << "Missing expected IR file.\n";
    exit(1);
  } else if (optind == argc - 1) {
    irFile = argv[optind];
  } else {
    WithColor::error() << "Expecting exactly 1 IR file.\n";
    exit(EXIT_FAILURE);
  }

  // message() will not printed anything if verbose mode is turned off since it
  // will check the verbose global variable internally itself. Adding the
  // check anyway for clarity.
  if (verbose)
    message() << "Enabled verbose mode\n";

  // Always check if the file could be read into memory.
  message() << "Reading LLVM IR from file: " << irFile << "\n";
  auto mbufOrErr = MemoryBuffer::getFile(irFile);
  if (!mbufOrErr) {
    std::error_code err = mbufOrErr.getError();
    WithColor::error() << err.message() << "\n";
    exit(err.value());
  }

  // The LLVM Context object must live at least as long as the last module that
  // uses it. Don't allocate it on the stack in case it gets too large.
  std::unique_ptr<LLVMContext> context(new LLVMContext());

  // Ah, the joys of C++. Objects wrapped inside objects wrapped inside objects.
  // It would put any matryoshka doll-maker to shame.
  std::unique_ptr<MemoryBuffer> mbuf = std::move(mbufOrErr.get());
  MemoryBufferRef mbufRef(*mbuf);
  SMDiagnostic diag;

  message() << "Parsing LLVM IR\n";
  std::unique_ptr<Module> m = parseIR(mbufRef, diag, *context);
  if (!m) {
    WithColor::error() << diag.getMessage() << "\n";
    exit(EXIT_FAILURE);
  }
  message() << "Generated LLVM module from IR\n";

  int runStatus = runTool(*m);

  // runStatus != 0 indicates an error of some sort.
  if (runStatus) {
    WithColor::error() << "An error occurred while processing the module\n";
  } else if (outFile.size()) {
    std::error_code ec;
    llvm::raw_fd_ostream outf(outFile, ec, sys::fs::FileAccess::FA_Write);
    if (ec) {
      WithColor::error() << ec.message() << "\n";
    } else {
      outf << *m;
    }
  } else {
    outs() << *m << "\n";
  }

  return runStatus;
}

// ---------------------------- Example code ----------------------------

// The code in this section is just an example of how you can iterate over the
// contents of a module. If you are not very familiar with LLVM's API, it may
// be worthwhile taking a look at it to get a (very brief) taste of what you
// can do with it.
//
// There are, regrettably, a number of modern C++'isms within the example
// code. It was a deliberate choice because you are likely to encounter this
// more and more as you work with the LLVM code base. So it is perhaps best to
// get familiar with it.

// Do something with the given global variable.
int runOnGlobalVariable(GlobalVariable &g) {
  // In LLVM, global variables need not have a name. This typically the case
  // when the compiler promotes something that might seem "local" to the
  // programmer to a global because of the semantics of the language. For
  // instance, local variables with a SAVE attribute in Fortran are sometimes
  // promoted to globals.
  if (g.hasName())
    outs() << g.getName() << "\n";
  else
    // The type of a global variable is always a pointer in LLVM. To get the
    // type of the actual global, use getValueType().
    outs() << "Anonymous global with type " << g.getValueType() << "\n";

  return EXIT_SUCCESS;
}

// Do something interesting with the given global alias.
int runOnGlobalAlias(GlobalAlias &a) {
  outs() << a.getName() << " aliases to " << a.getAliasee()->getName() << "\n";
  return EXIT_SUCCESS;
}

// Do something with the given function.
int runOnFunction(Function &f) {
  // All functions have names - even anonymous ones.
  outs() << f.getName() << "\n";

  // The function body consists of basic blocks.
  outs() << "  " << std::distance(f.begin(), f.end()) << " basic blocks\n";

  // There are two ways of iterating over instructions in a function. This is
  // one way where you simply iterate over all the instructions in the function
  // without bothering about the basic block in which they are contained.
  outs() << "  " << std::distance(inst_begin(f), inst_end(f))
         << " instructions\n";

  // The approach that you are more likely to see in various analyses and
  // optimizations is where you iterate over all the basic blocks in the
  // function and on each instruction within a basic block. Which approach you
  // choose depends on what you are trying to do.
  size_t cond = 0;
  for (BasicBlock &bb : f)
    for (Instruction &inst : bb)
      // This is also a pattern that you are very likely to encounter when
      // analyzing/transforming code. You will visit each instruction and
      // depending on the type of the instruction, handle it as necessary. You
      // are not required to handle every instruction type, obviously.
      //
      // There is a newer approach in LLVM that is now recommended which is
      // the use of an InstVisitor class which eliminates such switch
      // statements. For newer code, that method should be preferred, although
      // if there are only one or two instructions to be handled, this "older"
      // approach is just fine.
      if (auto *br = dyn_cast<BranchInst>(&inst))
        if (br->getCondition())
          ++cond;

  outs() << "  " << cond << " instructions are conditional branches\n";

  return EXIT_SUCCESS;
}

int runOnModule(Module &m) {
  message() << "Running on module\n";

  // For now, all we will do is print some metadata about the module itself and
  // all of the top-level entities in it.

  // First. some metadata about the module.
  outs() << "Name: " << m.getName() << "\n";

  // The triple encodes some system-level information about the target for
  // which the code is being compiled. This could include the architecture,
  // vendor, architecture sub-type, operating system, system environment, and
  // object format. Not all of these parameters will necessarily be present in
  // the triple (there used to be only three fields at one point, so the name
  // "triple" was actually relevant, but as with all of these things, it got
  // inflated over time).
  outs() << "Target: " << m.getTargetTriple() << "\n";

  // And then on to the top-level entities in the module.

  // Global variables.
  outs() << "Found " << std::distance(m.global_begin(), m.global_end())
         << " global variables\n";
  for (GlobalVariable &g : m.globals())
    runOnGlobalVariable(g);
  outs() << "\n";

  // Aliases.
  outs() << "Found " << std::distance(m.alias_begin(), m.alias_end())
         << " aliases\n";
  for (GlobalAlias &a : m.aliases())
    runOnGlobalAlias(a);
  outs() << "\n";

  // Function declarations.
  outs() << "Found " << std::count_if(m.begin(), m.end(), [](Function &f) {
    return f.empty();
  }) << " function declarations\n";
  for (Function &f : m)
    if (f.empty())
      outs() << f.getName() << "\n";
  outs() << "\n";

  // Function definitions.
  // This delightful bit of C++ sophistry with std::bind is to pass the member
  // function as the third argument to std::count_if. The placeholder indicates
  // that std::count_if will provide the argument that will be passed to size
  // (since size is a member function, that argument is the object on which
  // size is called). Couldn't we have just used a lambda like we did above?
  // Of course we could have. So why didn't we? Because we can. And why not
  // make a fraught situation worse by posturing with more arcane C++ that only
  // makes sense if you squint hard enough!
  outs() << "Found "
         << std::count_if(m.begin(), m.end(),
                          std::bind(&Function::size, std::placeholders::_1))
         << " function definitions\n";
  for (Function &f : m)
    if (f.size())
      runOnFunction(f);
  outs() << "\n";

  return EXIT_SUCCESS;
}

// ------------------------- End of example code ---------------------------
