*LLVM IR TOOL*

This is a proof-of-concept, just to show how to build a tool that operates on 
LLVM IR. This can be used when an LLVM IR file already exists and needs to be 
processed without having to modify LLVM or write an LLVM pass. In most cases, 
you actually do want a pass, but if you are certain that you don't, this can 
help with all the boilerplate that needs to be written. Most of this can
obviously be learned by going through the LLVM documentation, but it is not 
always obvious where to look.

The code also provides a trivial example of iterating over the contents of an 
LLVM module. It may be worth taking a look at if you are not familiar with the
LLVM API because it can give you a (very small) taste of what is possible. 

Both the source and `CMakeLists.txt` have been commented. Since this is 
intended to be a teaching tool, the comments there may actually be more useful
than the tool itself. In particular, there are build flags for CMake that you 
may have to set to get everything to work.

**Pre-requisites**

The following are required to build this tool:

    - CMake (3.18 or later. Earlier versions might work too)
    - A modern C++ compiler (with C++17 support)
    - LLVM version 15.0 (other versions may not work)
    - ninja/make

**Building**

Building in a directory other than the source is recommended. 

The following is a rough outline of how to build the tool.

```
    $ git clone git@gitlab.com:tarunprabhu/ir-tool.git
    $ mkdir build
    $ cd build
    $ cmake [OPTIONS] ../ir-tool/
```

You may need to use the `WITH_LLVM` option if LLVM is installed in a directory
that CMake does not know about. See the checked out `CMakeLists.txt` file for 
more details. (You really should be looking at *all* the source - including for 
the build files ;)).

```
    $ cmake -DWITH_LLVM=/path/to/llvm/install/root ../ir-tool/
```

For more information on `cmake` configure-time options
and how to use them, use their [documentation](https://cmake.org/cmake/help/latest/manual/cmake.1.html).

After configuring the build with `cmake`, build the actual tool.

```
    $ make
```

This will create an executable called `ir-tool` in the build directory. The 
tool expects a single LLVM IR file as input. Both bitcode (`.bc`) and LLVM
assembly (`.ll`) files are supported. Run `ir-tool` with `-h` for more options.

```
    $ ir-tool -h
```

The command above may be useful. But really, you should be reading the code :)
